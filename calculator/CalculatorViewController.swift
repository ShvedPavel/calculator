//
//  CalculatorViewController.swift
//  calculator
//
//  Created by Apple on 30.07.20.
//

import UIKit

enum Operations: String {
    case plus
    case minus
    case divide
    case multiple
    
    init?(from buttonTitle: String) {
        switch buttonTitle {
            case "+": self = .plus
            case "-": self = .minus
            case "x": self = .multiple
            case "/": self = .divide
            default: return nil
        }
    }
    
    func action(x: Double, y: Double) -> Double {
        switch self {
        case .plus:
            return x + y
        case .minus:
            return x - y
        case .divide:
            return x / y
        case .multiple:
            return x * y
        }
    }
}

class CalcuatorViewController: UIViewController {
    var x: Double = 0
    var operation: Operations? = nil
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func valueClicker(_ sender: UIButton) {
        guard var value = resultLabel.text, let enterValue = sender.currentTitle else {
            return
        }
        print(value)
        if value == "0" {
            resultLabel.text = enterValue
        } else {
            value += enterValue
            resultLabel.text = value
        }
    }
    
    @IBAction func resetbtn(_ sender: Any) {
        resultLabel.text = "0"
        x = 0
        operation = .plus
    }
    
    @IBAction func showResultBtn(_ sender: Any) {
        guard let secondValue = resultLabel.text else {
            return
        }
        guard let y = Double(secondValue) else {
            return
        }
        let result = operation?.action(x: x, y: y)
        resultLabel.text = String(result!)
    }
    
    @IBAction func actionBtn(_ sender: UIButton) {
        guard let operationTitle = sender.currentTitle else {
            return
        }
        guard let currentinput = resultLabel.text else {
            return
        }
        guard let xValue = Double(currentinput) else {
            return
        }
        operation = Operations(from: operationTitle)
        x = xValue
        resultLabel.text = ""
    }
}
